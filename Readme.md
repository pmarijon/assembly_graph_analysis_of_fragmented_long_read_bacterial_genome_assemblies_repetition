# Assembly graph analysis of fragmented long-read bacterial genome assemblies

## Requirements

- Canu 1.7
- minimap2
- miniasm 0.2-r128
- assembly_report https://gitlab.inria.fr/pmarijon/assembly_report version b583f4c427df8ad33926a0ab3a18377796bfce13
- SRA_dow_assembly https://gitlab.inria.fr/pmarijon/SRA_dow_assembly version dfef00bcbc6618ea20a5721bcffac4a83dced573
- python >= 3:

### python requirements

run :

```
pip install -r requirements.txt
```

## Dataset

### failled.lst

We selected a set of 630 datasets for which the NCTC assembly contains more than one chromosomal or unidentified contigs.
We assembled 130 of those datasets(arbitrarily selected) with Canu, using coverage-dependent parameters recommended in the manual.
Among those 130 assemblies, we selected 10 datasets that have mediocre N50 (between 0.7 Mb and 4.5 Mb).
In addition, we considered all 10 datasets tagged as "mis-assembled" by HINGE, and discarded 7 of them for which the Canu assembly had an expected number of contigs and genome size.
In total we selected 13 datasets, but note that this is only a subset of the unresolved NCTC datasets.

### explore.lst

3 dataset from failled.lst where NCTC assembly isn't too bad.

### 2_chromosome.lst

NCTC dataset where NCTC solved assembly with 2 chromosome expected chromosome.

### solved.lst

NCTC dataset where NCTC solved assembly, HINGE failled and Canu failled.

### contig_break.lst

HINGE failled and Canu failled cleanup manualy

## Repetition step

1. Download dataset with `./script/download_NCTC_pipeline.py ${your_file_list}`
2. Run assembly with SRA_dow_assembly `./script/SRA_dow_assembly -i ${your_file_list} -d data -a assembly -j NCTC.json`
3. Run assembly_report on each assembly `./script/run_assembly_report ${your_file_list}`
4. Run contig assignation `./script/blaster.py ${your_file_list}` blast xml file are saved in `assembly/${NCTC_id}/canu` directory, a file contig `taxonomic_assign.lst` in `assembly/${NCTC_id}` directory (for minimap run a script modification are required)
5. Run contig classification (containment or not) `./script/contig_classification.sh ${your_file_list}` this script make a mapping of contig against contig in file `assembly/${NCTC_id}/${NCTC_id}.paf` and `assembly/${NCTC_id}/contig2contig.lst`
6. Run filtering of path find run `./script/filter_contain_nogenomic.py ${your_file_list}` this script create file `assembly/${NCTC_id}/assembly_report/path_{canu|minimap}_extremity_result_filtred.csv`
7. Count short and long path and threshold `./script/long_short.py ${your_file_list}` the result of script are present in standard output.

## Other script

- `script/gfaminiasm2fasta.py` convert miniasm gfa in fasta file
- `script/venn_of_common_path.py` generate venn diagrame of path find for each path
- `script/subset_gfa.py` generate a new gfa with a subset of longest read

