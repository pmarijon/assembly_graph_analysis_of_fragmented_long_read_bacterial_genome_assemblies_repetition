#!/usr/bin/env python3

import csv
import begin
import pysam

def reformat_tigname(tigname):
    return tigname[3:].lstrip("0").strip()

def get_not_valid_contig(id_NCTC):
    remove = set()
    with open("assembly/"+id_NCTC+"/contig2contig.lst") as fh:
        for line in fh:
            remove.add(reformat_tigname(line))

    with open("assembly/"+id_NCTC+"/taxonomic_assign.lst") as fh:
        for line in fh:
            tig_name, tig_type = line.strip().split(";")
            if tig_type != "genomic":
                remove.add(reformat_tigname(tig_name))

    return remove

@begin.start
def main(nctc_file):
    for id_NCTC in open(nctc_file):
        id_NCTC = id_NCTC.strip()

        not_valid_contig = get_not_valid_contig(id_NCTC)
        previous_end = 0
        contig_order = list()
        for mapping in pysam.AlignmentFile("assembly/"+ id_NCTC +"/contig2ref.bam").fetch():
            if mapping.query_name in not_valid_contig:
                continue
            contig_order.append(mapping.reference_start - previous_end)
            contig_order.append(mapping.query_name)
            previous_end = mapping.reference_end

        print(" ".join([str(i) for i in contig_order]))
