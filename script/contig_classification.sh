for dataset in $(cat $1)
do
    echo $dataset
    minimap2 -x asm10 -X assembly/$dataset/canu/canu.contigs.fasta assembly/$dataset/canu/canu.contigs.fasta > assembly/${dataset}/contig2contig.paf 2> /dev/null
    ./script/parse_paf.py assembly/${dataset}/contig2contig.paf > assembly/${dataset}/contig2contig.lst 
    minimap2 -x asm10 -X assembly/$dataset/miniasm/miniasm.fasta assembly/$dataset/miniasm/miniasm.fasta > assembly/${dataset}/contig2contig_miniasm.paf 2> /dev/null
    ./script/parse_paf.py assembly/${dataset}/contig2contig_miniasm.paf > assembly/${dataset}/contig2contig_miniasm.lst 
done
