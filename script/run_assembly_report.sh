#!/bin/bash

for i in $(cat $1)
do
    assembly_report -i assembly/${i}/canu -m assembly/${i}/minimap/minimap.paf -M assembly/${i}/miniasm/miniasm.gfa -o assembly/${i}/assembly_report
done
