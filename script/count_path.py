#!/usr/bin/env python3

import os
import csv
import sys
import pysam
import argparse

from collections import defaultdict, Counter

import networkx as nx

no_truth = {
    "NCTC5050",
    "NCTC9078",
    "NCTC7152",
}

truth = {
    "NCTC10006": [('55_end', '55_begin'), ('1802_end', '1802_begin')],
    "NCTC10332": [('1_begin', '1_end'), ('76_end', '76_begin'), ('49_end', '49_begin')],
    "NCTC10444": [('105_begin', '105_end'), ('671_begin', '671_end'), ('673_begin', '673_end'), ('675_end', '675_begin')],
    "NCTC10702": [('1_begin', '1_end'), ('2_begin', '2_end')],
#   MISASSMEBLY
#    "NCTC10794": [('4951_begin', '4951_end'), ('189_end', '189_begin'), ('6_end', '6_begin'), ('40_end', '40_begin'), ('190_begin', '190_end'), ('3_end', '3_begin'), ('4952_begin', '4952_end'), ('189_begin', '189_end'), ('_begin', '_end'), ('_begin', '_end')]
#   CANU SOLVED
#    "NCTC11473": []
    "NCTC12123": [('1_begin', '1_end'), ('2_begin', '2_end'), ('9_begin', '9_end')],
    "NCTC12132": [('1_begin', '1_end'), ('2_end', '2_begin')],
#   MISASSEMBLY
#    "NCTC12999": []
    "NCTC13125": [('3_begin', '3_end'), ('408_begin', '408_end'), ('1_end', '1_begin')], # Petit problème a voir peut_endêtre pas de réference NCTC valid
#   CANU SOLVED
#    "NCTC13145": [],
    "NCTC13463": [('66_begin', '66_end'), ('68_end', '68_begin')],
#   MISASSEMBLY
#    "NCTC4672": [],
#   MISASSEMBLY
#    "NCTC5053": [('_end', '_begin'), ('_end', '_begin'), ('_begin', '_end'), ('_begin', '_end'), ('_end', '_begin'), ('_begin', '_end')], # Petit pb a voir peut_endêtre pas de ref NCTC valid
    "NCTC5055": [('1791_begin', '1791_end'), ('1789_begin', '1789_end')],
#    "NCTC8179": [('141_begin', '141_end'), ('6_begin', '6_end'), ('147_end', '147_begin'), ('145_end', '145_begin'), ('1523_end', '1523_begin'), ('12_end', '12_begin'), ('147_begin', '147_end'), ('143_begin', '143_end'), ('141_begin', '141_end')], # petit misassembly
    "NCTC9075": [('1_begin', '1_end'), ('2930_begin', '2930_end'), ('129_end', '129_begin'), ('14_end', '14_begin'), ('55_begin', '55_end'), ('196_end', '196_begin')],
    "NCTC9078": [('1_begin', '1_end'), ('51_begin', '51_end')],
#   MISASSEMBLY probable
#    "NCTC9111": [('1_begin', '1_end'), ('54_end', '54_begin'), ('63_end', '63_begin')],
#   MISASSEMBLY probable
#    "NCTC9098": [('1_begin', '1_end'), ('526_begin', '526_end'), ('528_begin', '528_end'), ('530_begin', '530_end')]
    "NCTC9112": [('707_begin', '707_end'), ('705_end', '705_begin'), ('65_begin', '65_end'), ('84_end', '84_begin')],
    "NCTC9596": [('76_begin', '76_end'), ('3_begin', '3_end'), ('4_begin', '4_end'), ('78_begin', '78_end'), ('74_begin', '74_end')]
}

def main(args):
    parser = argparse.ArgumentParser(prog="assembly_report",
                                     formatter_class=argparse.
                                     ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--input",
                        help="File contain on SRA accession sample per line",
                        type=argparse.FileType("r"), required=True)
    parser.add_argument("-o", "--output",
                        help="File where image are write",
                        type=argparse.FileType("w"), required=True)

    args = vars(parser.parse_args(args))
    run(args["input"], args["output"])

    
def run(input_file, output):
    
    print(",".join(["NCTC ID",
                    "contig canu",
                    "dead end",
                    "dead end solved",
                    "all path",
                    "theorical path",
                    "discard",
                    "good",
                    "good no repeat",
                    "good repeat"]),
          file=output)

    precision_sensibility = Counter()
    summary = defaultdict(int)
    for nctc_id in input_file:
        nctc_id = nctc_id.strip()
       
        result = Counter()
        paths, ref, valid_contig = categorize_path(nctc_id)

        nb_contig = len(valid_contig)
        nb_extremity = nb_contig * 2

        extremity2pathqual = defaultdict(lambda: defaultdict(int))
        for key, val in paths.items():
            for a in key:
                if val["good_tig"] and val["good_len"]:
                    extremity2pathqual[a]["good"] += 1

        repeat = set()
        for key, val in extremity2pathqual.items():
            if val["good"] > 1:
                repeat.add(key)
                
        for (a, b), val in paths.items():
            prefix = ""
            
            prefix += "repeat_" if a in repeat or b in repeat else "norepeat_"
            
            if ref:
                prefix += "adjacent_" if val["adjacents"] else "noadjacent_"
            
            if not val["good_tig"] and not val["good_len"]:
                result[prefix + "bad"] += 1
            elif not val["good_tig"] or not val["good_len"]:
                result[prefix + "middle"] += 1
            else:
                result[prefix + "good"] += 1
                
        if ref:
            precision_sensibility.update(result)

        nb_dead_end_canu, canu_linked = get_nb_deadend(nctc_id, valid_contig) 

        nb_conti = nb_contig
        nb_d_can = nb_dead_end_canu
        print(canu_linked)
        print(extremity2pathqual)
        print([norm_tigname(key) for key in extremity2pathqual.keys()])
        nb_l_noc = sum([1 for key, val in extremity2pathqual.items() if val["good"] > 0 and norm_tigname(key) not in canu_linked])
        all_path = sum([result[k] for k in result])
        theo_pat = int((nb_extremity * (nb_extremity - 2)) / 2)
        discard_ = sum([result[k] for k in result if "good" not in k])
        good____ = sum([result[k] for k in result if "good" in k])
        good_nor = sum([result[k] for k in result if "good" in k and "norepeat" in k])
        good_rep = sum([result[k] for k in result if "good" in k and "norepeat" not in k])

        summary["nb_conti"] += nb_conti
        summary["nb_d_can"] += nb_d_can
        summary["nb_l_noc"] += nb_l_noc
        summary["all_path"] += all_path
        summary["theo_pat"] += theo_pat
        summary["discard_"] += discard_
        summary["good____"] += good____
        summary["good_nor"] += good_nor
        summary["good_rep"] += good_rep
        summary["nb_id___"] += 1
        
        print(",".join([nctc_id,
                        str(nb_conti),
                        str(nb_d_can),
                        str(nb_l_noc),
                        str(all_path),
                        str(theo_pat),
                        str(discard_),
                        str(good____),
                        str(good_nor),
                        str(good_rep)]),
              file=output)
    for k, v in sorted(precision_sensibility.items(), key=lambda x: x[0]):
        print(k, ",", v)
  

    print(",".join(["Summary",
                    str(summary["nb_conti"] / summary["nb_id___"]),
                    str(summary["nb_d_can"] / summary["nb_id___"]),
                    str(summary["nb_l_noc"] / summary["nb_id___"]),
                    str(summary["all_path"] / summary["nb_id___"]),
                    str(summary["theo_pat"] / summary["nb_id___"]),
                    str(summary["discard_"] / summary["nb_id___"]),
                    str(summary["good____"] / summary["nb_id___"]),
                    str(summary["good_nor"] / summary["nb_id___"]),
                    str(summary["good_rep"] / summary["nb_id___"])]),
          file=output)


bam_path = "assembly/{}/contig2ref.bam" 
AAG_path = "assembly/{}/report_filtred_unbiased_AAG.csv"
def categorize_path(nctc_id):
    path2qual = defaultdict(dict)

    valid_contig = get_valid_contig(nctc_id)

    adjacents_tig = contig_order(nctc_id)
    
    ref = bool(adjacents_tig)

    with open(AAG_path.format(nctc_id)) as path_file:
        reader = csv.DictReader(path_file, delimiter=",")
        for row in reader:
            if norm_contigname(row["tig1"]) not in valid_contig or norm_contigname(row["tig2"]) not in valid_contig:
                continue

            tigA = row["tig1"]
            tigB = row["tig2"]
            
            key = frozenset({tigA, tigB})
            
            if row["nbread_contig"] == "not_found":
                continue

            if key in path2qual and path2qual[key]["length"] < int(row["nb_base"]):
                print("skip key {} len 1 {} len 2 {}".format(key, path2qual[key]["length"], row["nb_base"]), file=sys.stderr)
                continue

            path2qual[key]["length"] = int(row["nb_base"])
            path2qual[key]["good_tig"] = trust_tig(row["nbread_contig"], (norm_contigname(row["tig1"]), norm_contigname(row["tig2"])), valid_contig)
            path2qual[key]["good_len"] = trust_len(int(row["nb_base"]))
            path2qual[key]["adjacents"] = (norm_tigname(row["tig1"]), norm_tigname(row["tig2"])) in adjacents_tig

    return path2qual, ref, valid_contig


def contig_order(nctc_id):
    valid_junction = set()

    if nctc_id not in truth:
        return valid_junction

    for a, b in zip(truth[nctc_id], truth[nctc_id][1:]):
        valid_junction.add((a[1], b[0]))
        valid_junction.add((b[0], a[1]))

    valid_junction.add((truth[nctc_id][0][0], truth[nctc_id][-1][1]))
    valid_junction.add((truth[nctc_id][-1][1], truth[nctc_id][0][0]))

    return valid_junction


canu_graph_path = "assembly/{}/canu/canu.contigs.gfa"
def get_nb_deadend(nctc_id, contig):
    import gfapy

    gfa = gfapy.Gfa()
    for line in open(canu_graph_path.format(nctc_id)):
        try:
            gfa.add_line(line)
        except gfapy.Error as e:
            print("error {} nctc {} line {}".format(e, nctc_id, line), file=sys.stderr)
            continue
    
    graph = nx.DiGraph()
    for seg in gfa.segments:
        graph.add_node(norm_contigname(seg.name) + "+")
        graph.add_node(norm_contigname(seg.name) + "-")
    
    for dov in gfa.dovetails:
        graph.add_edge(
            norm_contigname(dov.from_segment.name) + dov.from_orient,
            norm_contigname(dov.to_segment.name) + dov.to_orient)
    graph = nx.transitive_closure(graph)

    for node in {n for n in graph.node if n[:-1] not in contig}:
        graph.remove_node(node)

    gfa = gfapy.Gfa()
    gfa.add_line("H\tVN:Z:1.0")
    for node in {n[:-1] for n in graph.node}:
        gfa.add_line("S\t{}\t*".format(node))
    for edge in graph.edges:
        n_from = edge[0][:-1]
        o_from = edge[0][-1]
        n_to = edge[1][:-1]
        o_to = edge[1][-1]
        gfa.add_line("L\t{}\t{}\t{}\t{}\t*".format(n_from, o_from, n_to, o_to))

    nb_deadend = 0
    for s in gfa.segments:
        if len(s.dovetails_L) == 0:
            nb_deadend += 1
        if len(s.dovetails_R) == 0:
            nb_deadend += 1

    canu_linked = set()
    for dov in gfa.dovetails:
        
        if dov.from_orient == "+" and dov.to_orient == "+":
            canu_linked.add(dov.from_segment.name + "_end")
            canu_linked.add(dov.to_segment.name + "_begin")
        elif dov.from_orient == "+" and dov.to_orient == "-":
            canu_linked.add(dov.from_segment.name + "_end")
            canu_linked.add(dov.to_segment.name + "_end")
        elif dov.from_orient == "-" and dov.to_orient == "+":
            canu_linked.add(dov.from_segment.name + "_begin")
            canu_linked.add(dov.to_segment.name + "_begin")
        elif dov.from_orient == "-" and dov.to_orient == "-":
            canu_linked.add(dov.from_segment.name + "_begin")
            canu_linked.add(dov.to_segment.name + "_end")
    
    return nb_deadend, canu_linked

tig_path = "assembly/{}/canu/canu.contigs.fasta"
taxonomic_assign_path = "assembly/{}/taxonomic_assign.lst"
def get_valid_contig(nctc_id):
    valid_tig = dict()

    from Bio import SeqIO
    for record in SeqIO.parse(tig_path.format(nctc_id), "fasta"):
        if len(record.seq) > 100000:
            valid_tig[norm_contigname(record.id)] = len(record.seq)

    import csv
    with open(taxonomic_assign_path.format(nctc_id), "r") as fh:
        reader = csv.reader(fh, delimiter=";")
        for row in reader:
            if norm_contigname(row[0]) in valid_tig and row[1] != "genomic":
                del valid_tig[norm_contigname(row[0])]

    return valid_tig


def trust_len(length):
    if length >= 10000:
        return False
    else:
        return True


def trust_tig(nb_contig, pair_tig, valid_tig):

    nb_contig = nb_contig.split(";")
    for contig in nb_contig:
        contig = contig.split(":")
        if norm_contigname(contig[0]) in valid_tig and norm_contigname(contig[0]) not in pair_tig:
            return False

    return True


def norm_contigname(tigname):
    tigname = tigname.split("_")[0]
    tigname = tigname[3:]
    tigname = tigname.lstrip('0')
    return tigname

def norm_tigname(tigname):
    tigname = tigname[3:]
    tigname = tigname.lstrip('0')
    return tigname

if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
