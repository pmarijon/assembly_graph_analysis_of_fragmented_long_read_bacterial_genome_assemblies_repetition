#!/usr/bin/env python3

import csv
import pysam
import begin

tigInfo_template = "assembly/{}/canu/canu.contigs.layout.tigInfo"
minimap_gfa_template = "assembly/{}/miniasm/miniasm.gfa"

extremity_template = "assembly/{}/assembly_report/find_path/path_{}_extremity_result.csv"
#extremity_template = "assembly/{}/assembly_report/path_{}_extremity_result_filtred.csv"

def get_short_long(filename, threshold):
    
    tig2dist = dict()
    with open(filename, "r") as fh:
        for row in csv.DictReader(fh):
            tig2dist[(row["tigA"], row["tigB"])] = int(row["path_len"])
    
    for k, v in tig2dist.items():
        if v <= threshold:
            tig2dist[k] = "short"
        else:
            tig2dist[k] = "long"

    return tig2dist


def get_long_short_canu(dataset):
    
    threshold = float("inf")
    with open(tigInfo_template.format(dataset), "r") as fh:
        for row in csv.DictReader(fh, delimiter='\t'):
            if row["tigClass"] == "contig" and row["coverage"] != "1.00":
                if int(row["numChildren"]) < threshold:
                    threshold = int(row["numChildren"])


    if threshold < 10: threshold = 10 

    return get_short_long(extremity_template.format(dataset, "canu"), threshold), threshold


def get_long_short_minimap(dataset):
    threshold = 5

    tig2len = dict()
    name = "remove"
    nb = 0
    with open(minimap_gfa_template.format(dataset), "r") as fh:
        for line in fh:
            row = line.split('\t')
            if row[0] == "S":
                tig2len[name] = nb
                name = row[1]
                nb = 0
            elif row[1]:
                nb += 1

    del tig2len["remove"]
    
    threshold = float("inf")
    for tig, nb in tig2len.items():
        if nb > 1 and nb < threshold:
            threshold = nb
    
    if threshold < 10: threshold = 10 

    return get_short_long(extremity_template.format(dataset, "minimap"), threshold), threshold

def reformat_tigname(tigname):
    return tigname[3:].lstrip("0").strip()

def get_not_valid_contig(id_NCTC):
    remove = set()
    with open("assembly/"+id_NCTC+"/contig2contig.lst") as fh:
        for line in fh:
            remove.add(reformat_tigname(line))

    with open("assembly/"+id_NCTC+"/taxonomic_assign.lst") as fh:
        for line in fh:
            tig_name, tig_type = line.strip().split(";")
            if tig_type != "genomic":
                remove.add(reformat_tigname(tig_name))

    return remove

def contigs_adjacents(id_NCTC):

    not_valid_contig = [] #get_not_valid_contig(id_NCTC)

    first, last = None, None
    previous = None

    contig_adjacents = list()
    for mapping in pysam.AlignmentFile("assembly/"+ id_NCTC +"/contig2ref.bam").fetch():
        query_name = reformat_tigname(mapping.query_name)
        if query_name in not_valid_contig:
            continue

        if previous is None:
            previous = query_name
            first = previous
            continue
        else:
            contig_adjacents.append((previous, query_name))
            contig_adjacents.append((query_name, previous))
            previous = query_name

        last = query_name
   
    contig_adjacents.append((first, last))
    contig_adjacents.append((last, first))

    return contig_adjacents

def match(key, minimap, canu, contigue, conditions):
    if minimap[key] == conditions[0] and \
            canu[key] == conditions[1] and \
            (key in contigue) == conditions[2]:
        return key
    return None


@begin.start
def main(nctc_file):

    for d in open(nctc_file):
        d = d.strip()

        tig2path_type_canu, thresh_canu = get_long_short_canu(d)
        tig2path_type_mini, thresh_mini = get_long_short_minimap(d)
       
        adjacents = contigs_adjacents(d)

        result = dict()
        for k in zip(["short"]*4+["long"]*4, ["short"]*2+["long"]*2+["short"]*2+["long"]*2, [True, False]*4):
            result[k] = []
        
        for k in set(set(tig2path_type_canu.keys()) | set(tig2path_type_mini.keys())):

            # If we didn't find path the path is long
            if k not in tig2path_type_mini:
                tig2path_type_mini[k] = "long"
            if k not in tig2path_type_canu:
                tig2path_type_canu[k] = "long"

            for conditions in result:
                result[conditions].append(match(k, tig2path_type_mini, tig2path_type_canu, adjacents, conditions))

        print(d, thresh_mini, thresh_canu)
        [print("\t", k, [u  for u in v if u]) for k, v in result.items()]
            
