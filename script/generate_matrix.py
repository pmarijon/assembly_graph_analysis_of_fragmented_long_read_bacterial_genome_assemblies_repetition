#!/usr/bin/env python3

# standard import
import sys
import csv
import begin
import pandas

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()

from enum import Enum
from collections import defaultdict, Counter

# bio import
import pysam

def __get_map_pos(filename, select):
    mapping = pysam.AlignmentFile(filename, "rb")
    ref2length = {ref: mapping.get_reference_length(ref) for ref in mapping.references}
    selected_ref = sorted(ref2length.items(), key=lambda x: x[1], reverse=True)[0][0] 
    for read in mapping.fetch(reference=selected_ref):
        if read.mapping_quality != 60:
            continue
        if read.flag & 2048:
            continue
        else:
            pass

        if select:
            if read.query_name in select:
                yield read.query_name, read.reference_start, read.reference_end, read.query_length, ref2length[selected_ref]
        else:
            yield read.query_name, read.reference_start, read.reference_end, read.query_length, ref2length[selected_ref]

def __pos_in_tig(tig_pos, read_pos):
    def in_pos(tig_beg, tig_end, beg_read):
        return tig_beg < beg_read and tig_end > beg_read

    return any([in_pos(a, b, read_pos) for a, b in tig_pos])

def get_read_pos(filename, select, reads2tig, tig2pos):
    reads2pos = defaultdict(list)
   
    for k, beg, end, query_len, _ in __get_map_pos(filename, select):
        map_len = end - beg
        k = __clean_tigname(k)
        reads2pos[k] = beg

    return reads2pos

def get_contig_pos(filename):
    tig2pos = defaultdict(list)
    ref_len = 0

    for k, beg, end, query_len, reflen in __get_map_pos(filename, None):
        k = __clean_tigname(k)
        tig2pos[k].append((beg, end))
        ref_len = reflen

    return tig2pos, reflen

def __clean_tigname(name):
    return name.replace("tig", "").lstrip("0")

read2tig_template = "assembly/{}/canu/canu.contigs.layout.readToTig"
def parse_read_tig(nctc_id):
    with open(read2tig_template.format(nctc_id)) as in_csv:
        reader = csv.DictReader(in_csv, delimiter='\t')
        for row in reader:
            if row["bgn"] > row["end"]:
                row["bgn"], row["end"] = row["end"], row["bgn"]
            yield row["#readID"], row["tigID"], row["bgn"], row["end"]


tig_info_template = "assembly/{}/canu/canu.contigs.layout.tigInfo"
def parse_tig_info(nctc_id):
    with open(tig_info_template.format(nctc_id)) as in_csv:
        reader = csv.DictReader(in_csv, delimiter='\t')
        for row in reader:
            if float(row["coverage"]) > 1.00:
                yield row["#tigID"], int(row["tigLen"])


readname_template = "assembly/{}/canu/unitigging/canu.gkpStore/readNames.txt"
def parse_canu_read_name(nctc_id):
    filename = readname_template.format(nctc_id)

    id2readname = dict()
    with open(filename, "r") as in_csv:
        reader = csv.reader(in_csv, delimiter='\t')
        for row in reader:
            id2readname[row[0]] = row[1].split(" ")[0]
    
    return id2readname

def __in_interval(a, b):
    if a[1] > b[0] and b[1] > a[1]:
        return True
    if a[0] > b[0] and b[1] > a[0]:
        return True

    return False

def __integrate_pos(positions, pos):
    if not positions:
        return 0
    
    for couple in positions:
        if __in_interval(couple, pos):
            return -1
    ordered_pos = sorted(list(positions + [pos]), key=lambda x: x[1])
    return ordered_pos.index(pos)

contig_template = "assembly/{}/contig2ref.bam"
raw_read_template = "assembly/{}/raw_read2ref.bam"
def get_ext_order(nctc_id, paths):

    # get all intresting read
    reads = set()
    for pairs in get_possible_path(nctc_id, "canu"):
        reads.add(pairs[0])
        reads.add(pairs[1])
    for pairs in get_possible_path(nctc_id, "minimap"):
        reads.add(pairs[0])
        reads.add(pairs[1])
    
    # Get info about read
    id2read = parse_canu_read_name(nctc_id)
    reads_tig_info = [(i, j, k, l) for i, j, k, l in parse_read_tig(nctc_id)]
    reads2tig = {id2read[id]: tig for id, tig, _, _ in reads_tig_info}

    tig2reads = defaultdict(set)
    for read in reads:
        tig2reads[reads2tig[read]].add(read)

    reads2tigpos = {id2read[id]: int(beg) for id, _, beg, end in parse_read_tig(nctc_id)}
    tig2pos, ref_len = get_contig_pos(contig_template.format(nctc_id))
    reads2refpos = get_read_pos(raw_read_template.format(nctc_id), reads, reads2tig, tig2pos)

    tig2len = {tig: length for tig, length in parse_tig_info(nctc_id)}
    positions = list()
    ext2pos = dict()
    reads2ext = dict()
    cumulative_size = 0
    total_size = sum(tig2len.values())
    for tig, length in sorted(tig2len.items(), key=lambda x: x[1], reverse=True):
        # check if we can insert the tig
        if tig not in tig2pos:
            continue

        tig_pos = tig2pos[tig][0]

        integrate_pos = __integrate_pos(positions, tig_pos)
        if integrate_pos == -1:
            break
        
        positions.insert(integrate_pos, tig_pos)
        ext2pos[tig + "l"] = tig_pos[0]
        ext2pos[tig + "r"] = tig_pos[1]

        # associate each read to her extremity
        for read in tig2reads[tig]:
            if reads2tigpos[read] < tig2len[tig]/2:
                reads2ext[read] = tig + "l"
            else:
                reads2ext[read] = tig + "r"

        # stop
        cumulative_size += length
        if cumulative_size > total_size * 0.95:
            break

    return ext2pos, reads2ext, ref_len

extremity_search_template = "assembly/{}/assembly_report/find_path/path_{}_extremity_search.csv"
def get_possible_path(nctc_id, mapper):

    with open(extremity_search_template.format(nctc_id, mapper)) as canu_fh:
        reader = csv.DictReader(canu_fh)
        for row in reader:
            yield row["readA"][:-1], row["readB"][:-1]


extremity_result_template = "assembly/{}/assembly_report/find_path/path_{}_extremity_result.csv"
def get_path(nctc_id):
    # (readA, readB) -> {"canu": len_canu, "minimap": len_minimap}
    paths = dict()
    with open(extremity_result_template.format(nctc_id, "canu")) as canu_fh:
       reader = csv.DictReader(canu_fh)
       for row in reader:
            key = (row["readA"][:-1], row["readB"][:-1])
            if key not in paths:
               paths[key] = dict()
               paths[key]["canu"] = int(row["path_len"])
            else:
                if paths[key]["canu"] < int(row["path_len"]):
                    paths[key]["canu"] = int(row["path_len"])
   
    with open(extremity_result_template.format(nctc_id, "minimap")) as minimap_fh:
       reader = csv.DictReader(minimap_fh)
       for row in reader:
            key = (row["readA"][:-1], row["readB"][:-1])
            if key not in paths:
               paths[key] = dict()
               paths[key]["minimap"] = int(row["path_len"])
            else:
                if "minimap" not in paths[key]:
                    paths[key]["minimap"] = int(row["path_len"])
                if paths[key]["minimap"] < int(row["path_len"]):
                    paths[key]["minimap"] = int(row["path_len"])

    return paths

class Match2Value(Enum):
    SHORT_MINIMAP = 0b1
    SHORT_CANU    = 0b10
    LONG_MINIMAP  = 0b100
    LONG_CANU     = 0b1000
    CONTIG_CLOSE  = 0b10000
    CONTIG_FAR    = 0b100000

    def __int__(self):
        return self.value

    def __float__(self):
        return float(self.value)

    def __str__(self):
        ret = list()
        self = int(self)

        if self == 0:
            return "_"

        if self & Match2Value.SHORT_MINIMAP.value:
            ret.append("SM")
        if self & Match2Value.SHORT_CANU.value:
            ret.append("SC")
        if self & Match2Value.LONG_MINIMAP.value:
            ret.append("LM")
        if self & Match2Value.LONG_CANU.value:
            ret.append("LC")
        if self & Match2Value.CONTIG_CLOSE.value:
            ret.append("CC")
        if self & Match2Value.CONTIG_FAR.value:
            ret.append("CF")
        
        return " ".join(ret)

tigInfo_template = "assembly/{}/canu/canu.contigs.layout.tigInfo"
def get_canu_threshold(dataset):
    
    threshold = float("inf")
    with open(tigInfo_template.format(dataset), "r") as fh:
        for row in csv.DictReader(fh, delimiter='\t'):
            if row["tigClass"] == "contig" and row["coverage"] != "1.00":
                if int(row["numChildren"]) < threshold:
                    threshold = int(row["numChildren"])


    if threshold < 10: threshold = 10 

    return threshold

minimap_gfa_template = "assembly/{}/miniasm/miniasm.gfa"
def get_minimap_threshold(dataset):
    threshold = 5

    tig2len = dict()
    name = "remove"
    nb = 0
    with open(minimap_gfa_template.format(dataset), "r") as fh:
        for line in fh:
            row = line.split('\t')
            if row[0] == "S":
                tig2len[name] = nb
                name = row[1]
                nb = 0
            elif row[1]:
                nb += 1

    del tig2len["remove"]
    
    threshold = float("inf")
    for tig, nb in tig2len.items():
        if nb > 1 and nb < threshold:
            threshold = nb
    
    if threshold < 10: threshold = 10 

    return threshold

def build_matrix(nctc_id, ext2pos, read2ext, paths):
    extorder = sorted(ext2pos.keys(), key=lambda x: ext2pos[x])
    matrix = pandas.DataFrame(data=np.zeros((len(extorder), len(extorder)), dtype=Match2Value),
                              index=extorder,
                              columns=extorder)

    canu_thres = get_canu_threshold(nctc_id)
    mini_thres = get_canu_threshold(nctc_id)
    
    for readA, readB in paths.keys():
        if readA not in read2ext:
            continue
        if readB not in read2ext:
            continue

        tigA = read2ext[readA]
        tigB = read2ext[readB]


        if "canu" in paths[(readA, readB)]:
            if paths[(readA, readB)]["canu"] <= canu_thres:
               matrix[tigA][tigB] = matrix[tigA][tigB] | int(Match2Value.SHORT_CANU)
            else:
                matrix[tigA][tigB] = matrix[tigA][tigB] | int(Match2Value.LONG_CANU)

        if "minimap" in paths[(readA, readB)]:
            if paths[(readA, readB)]["minimap"] <= mini_thres:
                matrix[tigA][tigB] = matrix[tigA][tigB] | int(Match2Value.SHORT_MINIMAP)
            else:
                matrix[tigA][tigB] = matrix[tigA][tigB] | int(Match2Value.LONG_MINIMAP)

    return matrix

def __only_zero(values):
    return all([value == 0 for value in values])

def clean_matrix(matrix):
    rm_extremity = set()

    contigs = {col[:-1] for col in matrix.columns}

    for contig in contigs:
        contig_l = contig+"l"
        contig_r = contig+"r"

        empty = True
        if contig_l in matrix:
            empty = __only_zero(matrix[contig_l])
            if not empty:
                continue

        if contig_r in matrix:
            empty = __only_zero(matrix[contig_r])
            if not empty:
                continue

        values = {index: val for index, val in matrix.iterrows()}
        if contig_l in values:
            empty = __only_zero(values[contig_l])
            if not empty:
                continue

        if contig_r in values:
            empty = __only_zero(values[contig_r])
            if not empty:
                continue

        if contig_l in matrix.columns:
            rm_extremity.add(contig_l)
        if contig_r in matrix.columns:
            rm_extremity.add(contig_r)

    for ext in rm_extremity:
        matrix.drop(ext, axis=0, inplace=True)
        matrix.drop(ext, axis=1, inplace=True)

    return matrix

def add_distance(matrix, ext2pos, read2ext, paths, ref_len):
    extorder = sorted(ext2pos.keys(), key=lambda x: ext2pos[x])

    for readA, readB in paths.keys():
        if readA not in read2ext:
            continue
        if readB not in read2ext:
            continue

        tigA = read2ext[readA]
        tigB = read2ext[readB]

        if tigA not in matrix:
            continue
        if tigB not in matrix[tigA]:
            continue

#        ext_dist = abs(extorder.index(tigA) - extorder.index(tigB))
#        if ext_dist == 1 or ext_dist == len(extorder) - 1:
        a, b = (ext2pos[tigA], ref_len - ext2pos[tigB]) if ext2pos[tigA] < ext2pos[tigB] else (ext2pos[tigB], ref_len - ext2pos[tigA])
        c = abs(ext2pos[tigA] - ext2pos[tigB])
        ext_dist = a+b if a+b < c else c
        if ext_dist <= 25000:
            matrix[tigA][tigB] = matrix[tigA][tigB] | int(Match2Value.CONTIG_CLOSE)
        else:
            matrix[tigA][tigB] = matrix[tigA][tigB] | int(Match2Value.CONTIG_FAR)

    return matrix


def get_stat_on_matrix(matrix):
    counter = Counter()
    multi_sm_counter = 0

    for _, row in matrix.iterrows():
        nb_of_sm = 0
        for val in row:
            counter[val] += 1
            if val & int(Match2Value.SHORT_MINIMAP):
                nb_of_sm += 1
        if nb_of_sm > 1:
            multi_sm_counter += 1
    
    return counter, multi_sm_counter


base_dimension = (3.2, 2.4)
def save_matrix(matrix, path):
    matrix.fillna(-1, inplace=True)
    dims = [x * (len(matrix.columns) // 2) for x in base_dimension]
    fig, ax = plt.subplots(figsize=dims)

    ax = sns.heatmap(matrix,
                     #annot=matrix.applymap(lambda x: bin(x)),
                     annot=matrix.applymap(lambda x: Match2Value.__str__(x)),
                     fmt="s",
                     ax=ax)

    fig = ax.get_figure()
    fig.savefig(path)
    plt.clf()


@begin.start
def main(ids_file, prefix):
    global_counter = Counter()
    multi_sm_counter = 0
    for nctc_id in open(ids_file):
        nctc_id = nctc_id.strip()
        print(nctc_id)
        
        paths = get_path(nctc_id)
        
        ext2pos, read2ext, ref_len = get_ext_order(nctc_id, paths)
        if not ext2pos:
            continue

        matrix = build_matrix(nctc_id, ext2pos, read2ext, paths)

        matrix = clean_matrix(matrix)

        if matrix.empty:
            continue

        matrix = add_distance(matrix, ext2pos, read2ext, paths, ref_len)

        stat, sm_counter = get_stat_on_matrix(matrix)
        multi_sm_counter += sm_counter
        for key, val in stat.items():
            print("\t", Match2Value.__str__(key), val)
        
        global_counter.update(stat)

        save_matrix(matrix, prefix+nctc_id+".png")
    
    print("\nMulti SM", multi_sm_counter, "\n")
    
    tp, fp = 0, 0
    
    for key, val in global_counter.items():
        print(Match2Value.__str__(key), val)
        
        if key & int(Match2Value.SHORT_MINIMAP) and key & int(Match2Value.CONTIG_CLOSE):
            tp += val
        if key & int(Match2Value.LONG_MINIMAP) and key & int(Match2Value.CONTIG_FAR):
            tp += val
        if key & int(Match2Value.SHORT_MINIMAP) and key & int(Match2Value.CONTIG_FAR):
            fp += val
        if key & int(Match2Value.SHORT_MINIMAP) and key & int(Match2Value.CONTIG_FAR):
            fp += val

    print("TP", tp, "FP", fp)
