#!/usr/bin/env python3

import sys

to_remove = set()
for line in open(sys.argv[1]):
    line = line.strip().split()
    lengthA = int(line[1])
    lengthB = int(line[6])
    matching_bases = int(line[9]) # https://lh3.github.io/minimap2/minimap2.html
    query = line[0]
    target = line[5]
    ratioA = matching_bases*1.0/lengthA
    ratioB = matching_bases*1.0/lengthB
    
    if ratioA > 0.75:
        sys.stderr.write(query + " " + target + " " + str(ratioA) + '\n')
        to_remove.add(query)
    if ratioB > 0.75:
        sys.stderr.write(target + " " + query + " " + str(ratioB) + '\n')
        to_remove.add(target)

for x in to_remove: print(x)
