#!/usr/bin/env python3

import begin

@begin.start
def main(gfa, fasta):
    with open(fasta, "w") as fh_out, open(gfa) as fh:
        for line in fh:
            if line.startswith("S"):
                _, tig_id, tig_seq, _ = line.split("\t")
                fh_out.write(">{}\n{}\n".format(tig_id, tig_seq))
    
