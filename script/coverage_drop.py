#!/usr/bin/env python3

import sys
import argparse
import subprocess

canu_contig_template = "assembly/{}/contig2ref.bam"
miniasm_contig_template = "assembly/{}/contig2ref_miniasm.bam"


def main(args = None):
    
    if args is None:
        args = sys.argv[1:]

    parser = argparse.ArgumentParser(prog="assembly_report",
                                     formatter_class=argparse.
                                     ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--input",
                        help="File contain on SRA accession sample per line",
                        type=argparse.FileType("r"), required=True)

    args = vars(parser.parse_args(args))
    run(args["input"])

    return 0

def run(input_file):
    for nctc_id in input_file:
        nctc_id = nctc_id.strip()
        print(nctc_id)
        
        canu_drop = get_coverage_drop(canu_contig_template.format(nctc_id))
        miniasm_drop = get_coverage_drop(miniasm_contig_template.format(nctc_id))

        intersect = sorted(canu_drop & miniasm_drop)
        if len(intersect) == 0:
            continue
        
        begin = intersect[0]
        prev = begin
        for a in intersect:
            if  a - prev > 1:
                print("\t", begin, prev)
                begin = a
            prev = a

        print("\t", begin, prev)
#        print(canu_drop & miniasm_drop)

        
def get_coverage_drop(mapping_path):
    gaps = set()
    
    command = ["genomeCoverageBed", "-d", "-ibam"]
    command.append(mapping_path)

    for line in subprocess.run(command, stdout=subprocess.PIPE).stdout.decode("utf-8").split("\n"):
        line = line.strip().split("\t")
        if len(line) != 3:
            continue

        if line[2] == "0":
            gaps.add(line[1])


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
