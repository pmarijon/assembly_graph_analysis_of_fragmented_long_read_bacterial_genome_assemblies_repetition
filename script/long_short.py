#!/usr/bin/env python3

import begin
import csv

tigInfo_template = "assembly/{}/canu/canu.contigs.layout.tigInfo"
minimap_gfa_template = "assembly/{}/miniasm/miniasm.gfa"

extremity_template = "assembly/{}/assembly_report_new/find_path/path_{}_extremity_result.csv"

def get_short_long(filename, threshold):
    long_path = 0
    short_path = 0
    tig2dist = dict()
    with open(filename, "r") as fh:
        for row in csv.DictReader(fh):
            tig2dist[frozenset((row["readA"], row["readB"]))] = int(row["path_len"])
    
    for v in tig2dist.values():
        if v <= threshold:
            short_path += 1
        else:
            long_path += 1

    return short_path, long_path


def get_long_short_canu(dataset):
    
    threshold = float("inf")
    with open(tigInfo_template.format(dataset), "r") as fh:
        for row in csv.DictReader(fh, delimiter='\t'):
            if row["tigClass"] == "contig" and row["coverage"] != "1.00":
                if int(row["numChildren"]) < threshold:
                    threshold = int(row["numChildren"])


    if threshold < 10: threshold = 10 

    short_path, long_path = get_short_long(extremity_template.format(dataset,
                                                                     "canu"),
                                           threshold)

    return (dataset, str(short_path), str(long_path), str(threshold))


def get_long_short_minimap(dataset):
    threshold = 5

    tig2len = dict()
    name = "remove"
    nb = 0
    with open(minimap_gfa_template.format(dataset), "r") as fh:
        for line in fh:
            row = line.split('\t')
            if row[0] == "S":
                tig2len[name] = nb
                name = row[1]
                nb = 0
            elif row[1]:
                nb += 1

    del tig2len["remove"]
    
    threshold = float("inf")
    for tig, nb in tig2len.items():
        if nb > 1 and nb < threshold:
            threshold = nb
    
    if threshold < 10: threshold = 10 

    short_path, long_path = get_short_long(extremity_template.format(dataset,
                                                                     "minimap"),
                                           threshold)

    return (str(short_path), str(long_path), str(threshold))

@begin.start
def main(nctc_file):

    print(",".join(["dataset", "canu", "short", "long", "threshold", "minimap", "short", "long", "threshold"]))
    for d in open(nctc_file):
        d = d.strip()
        print(",".join(get_long_short_canu(d) + get_long_short_minimap(d)))
