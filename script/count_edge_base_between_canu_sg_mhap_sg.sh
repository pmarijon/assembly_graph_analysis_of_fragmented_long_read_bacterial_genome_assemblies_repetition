#/usr/bin/bash

echo "ID, base corrected, base raw, edge corrected, edge raw"

for i in $(cat $1)
do
    echo -n ${i}","
    echo -n $(cat assembly/${i}/corrected_read.fastq | paste - - - - | cut -f2 | wc -c)","
    echo -n $(grep -v ">" data/${i}/merged.fasta | wc | awk '{print $3-$1}')","
    echo -n $(grep -c "^L" assembly/${i}/canu/canu_paf_rename.gfa)","
    echo $(grep -c "^L" assembly/${i}/minimap/minimap_good.gfa)
done
