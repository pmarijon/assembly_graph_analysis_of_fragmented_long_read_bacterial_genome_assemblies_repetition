#!/usr/bin/env python3

import csv
import begin

def filter_extremity_file(id_NCTC, tools, remove):
    out_file = id_NCTC+"/assembly_report/path_{}_extremity_result_filtred.csv" 
    in_file = id_NCTC+"/assembly_report/find_path/path_{}_extremity_result.csv" 

    with open(out_file.format(tools), "w") as fh_out, \
         open(in_file.format(tools)) as fh_in:
        reader = csv.reader(fh_in)
        writer = csv.writer(fh_out)
        for row in reader:
            if not (row[0] in remove or row[2] in remove):
                writer.writerow(row)

def reformat_tigname(tigname):
    return tigname[3:].lstrip("0").strip()

def jobs(id_NCTC):
    remove = set()
    with open("assembly/"+id_NCTC+"/contig2contig.lst") as fh:
        for line in fh:
            remove.add(reformat_tigname(line))

    with open("assembly/"+id_NCTC+"/taxonomic_assign.lst") as fh:
        for line in fh:
            tig_name, tig_type = line.strip().split(";")
            if tig_type != "genomic":
                remove.add(reformat_tigname(tig_name))
    
    print(id_NCTC, remove)

    filter_extremity_file("assembly/"+id_NCTC, "canu", remove)
    filter_extremity_file("assembly/"+id_NCTC, "minimap", remove)

@begin.start
def main(nctc_file):
    for id_NCTC in open(nctc_file):
        jobs(id_NCTC.strip())

