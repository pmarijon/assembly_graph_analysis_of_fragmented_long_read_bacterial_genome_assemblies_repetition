#!/usr/bin/env python3

import csv
import begin

@begin.start
@begin.convert(subset=float)
def main(gfa, sub_gfa, subset=0.3):

    reads2len = dict()
    with open(gfa) as fh:
        reader = csv.reader(fh, delimiter='\t')
        for line in reader:
            if line[0] == "S":
                reads2len[line[1]] = int(line[3].split(":")[2])

    reads_sort_len = sorted(reads2len, key=lambda x: reads2len[x], reverse=True)

    selected_read = set(reads_sort_len[:round(len(reads_sort_len)*subset)])

    with open(gfa) as fh:
        with open(sub_gfa, "w") as sub:
            writer = csv.writer(sub, delimiter='\t') 
            reader = csv.reader(fh, delimiter='\t')

            for line in reader:
                if line[0] == "S":
                    if line[1] in selected_read:
                        writer.writerow(line)
                elif line[0] == "L":
                    if line[1] in selected_read and line[3] in selected_read:
                        writer.writerow(line)
                else:
                    writer.writerow(line)
