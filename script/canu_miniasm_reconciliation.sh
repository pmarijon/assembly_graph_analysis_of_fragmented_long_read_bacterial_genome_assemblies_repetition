#!/bin/bash

work () {
    ~/send-notification.sh "begin canu_minimap reconciliation $1"
    mkdir -p assembly/${1}/reconciliation/
    minimap2 -x map-pb -m 25 -n 2 assembly/${1}/canu/canu.contigs.filtred.fasta assembly/${1}/miniasm/miniasm.filtred.fasta > assembly/${1}/reconciliation/mapping.paf
    miniasm -1 -2 -s 1000 -c 0 assembly/${1}/reconciliation/mapping.paf > assembly/${1}/reconciliation/assembly.gfa
    fpa -o gfa1 -s -c -i assembly/${i}/reconciliation/overlap_graph.gfa assembly/${i}/reconciliation/mapping.paf 
    ~/send-notification.sh "end canu minimap reconciliation $1 with $?"
}


for i in $(cat $1)
do
    work ${i} &
done
