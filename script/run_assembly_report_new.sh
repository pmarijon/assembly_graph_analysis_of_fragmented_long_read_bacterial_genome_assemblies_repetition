#!/bin/bash

work () {
    ~/send-notification.sh "begin knot $1"
    knot -c assembly/${1}/canu/canu.contigs.filtred.fasta -g assembly/${1}/canu/canu.contigs.gfa -r data/${1}/merged.fasta -o assembly/${1}/report_filtred_unbiased -- --rerun-incomplete
    ~/send-notification.sh "end knot $1 code $?"
}


for i in $(cat $1)
do
    work ${i} &
    sleep 3600
done
