#!/usr/bin/env python3

import csv
import begin

from matplotlib import pyplot as plt

def read_path_len(id_NCTC, tools, coverage):
    in_file = id_NCTC+"/assembly_report/path_{}_extremity_result_filtred.csv" 

    with open(in_file.format(tools)) as fh_in:
        reader = csv.DictReader(fh_in)
        for row in reader:
            yield int(row["path_len"]) - 2 / int(coverage)

def jobs(file_handler):
    normalized_path_len = list()
    for line in file_handler:
        id_NCTC, coverage = line.split(",")
        normalized_path_len.extend(list(read_path_len(id_NCTC, "canu", coverage)))
        normalized_path_len.extend(list(read_path_len(id_NCTC, "minimap", coverage)))

    return sorted(normalized_path_len), len(set({round(i) for i in normalized_path_len}))
     

@begin.start
def main(id_coverage):

    with open(id_coverage) as fh_in:
        data, nb_bins = jobs(fh_in)
        plt.hist(data, bins=nb_bins, histtype='stepfilled')
        plt.show()
