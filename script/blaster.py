#!/usr/bin/python3
# -*- coding: utf-8 -*-:

from Bio.Blast import NCBIWWW
from Bio.Blast import NCBIXML
from Bio import SeqIO

import tempfile
import os
import shutil
import sys
import json


bact_dict = json.load(open("NCTC.json"))

skipped_tigs = {
    "NCTC11126" : [],
    "NCTC9078"  : [],
    "NCTC7152"  : [],
    "NCTC11131" : [],
    "NCTC13095" : [],
    "NCTC5050"  : [],
    "NCTC12993" : [],
    "NCTC11800" : [],
    "NCTC13543" : [],
    "NCTC12694" : [],
    "NCTC13348" : [],
    "NCTC10988" : [],
    "NCTC12841" : [],
    "NCTC11435" : [], 
    "NCTC10006" : [],
    "NCTC10089" : [],
    "NCTC10332" : [],
    "NCTC10444" : [],
    "NCTC10702" : [],
    "NCTC10974" : [],
    "NCTC11473" : [],
    "NCTC11474" : [],
    "NCTC12132" : [],
    "NCTC12998" : [],
    "NCTC13125" : [],
    "NCTC13145" : [],
    "NCTC13384" : [],
    "NCTC13463" : [],
    "NCTC4444" : [],
    "NCTC5055" : [],
    "NCTC7360" : [],
    "NCTC8179" : [],
    "NCTC8500" : [],
    "NCTC9075" : [],
    "NCTC9081" : [],
    "NCTC9098" : [],
    "NCTC9111" : [],
    "NCTC9112" : [],
    "NCTC9143" : [],
    "NCTC9695" : [],
    "NCTC10884" : [],
    "NCTC11067" : [],
    "NCTC11435" : [],
    "NCTC13647" : [],
}

def analyze_blast (contigs_file, path, species, genomic_by_length = []):
    print("# blast analysis")
    category = dict()
    records = list(SeqIO.parse(contigs_file, "fasta"))
    for record in records:
        print(record.name, "length", len(record))
        if record.name in genomic_by_length:
            category[record.name] = { 'plasmidic' : 0, 'genomic' : 50, 'undefined' : 0 }
        else:
            output_filename = os.path.join(path,"{}.xml".format(record.name))
            category[record.name] = { 'plasmidic' : 0, 'genomic' : 0, 'undefined' : 0 }
            try:
                result_handle = open(output_filename)
                blast_records = list(NCBIXML.parse(result_handle))
                assert (len(blast_records) == 1), "hum ... more blast records than queries"
                for block_aligns in blast_records:
                    alignments = list(block_aligns.alignments)
                    if alignments:
                        for al in alignments:
#                            print "\t%s %d %d" % (al.title,len(record),al.length)
                            if "plasmid" in al.title:
                                category[record.name]['plasmidic'] += 1
                            elif species in al.title:
                                category[record.name]['genomic'] += 1
                            elif species.split()[0] in al.title:
                                category[record.name]['genomic'] += 1
                            elif "complete genome" in al.title and al.length > 3000000:
                                category[record.name]['genomic'] += 1                                
                            else:
                                category[record.name]['undefined'] += 1
                        print("\t{}".format(category[record.name]))

    #                    for hsp in al.hsps:
    #                        print "\t\t%d %d" % (hsp.query_start, hsp.query_end)
    #                        print "\t\t%d %d" % (hsp.sbjct_start, hsp.sbjct_end)
    #                        print "\t\t%d" % (hsp.expect)
                    else:
                        print("No alignment found")
            except None:
                print("No Blast output for " + record.name)

    #with open(os.path.join(path,"..","taxonomic_assign.lst"),"w") as output_fileid:
    with open(os.path.join(path,"..","taxonomic_assign_miniasm.lst"),"w") as output_fileid:
        for record in records:
            s = sum([ category[record.name][k] for k in category[record.name] ])
            try:
                c = [ k for k in category[record.name] if ((category[record.name][k]*1.0/s) >= 0.8) ]
                if len(c) == 1:
                    output_fileid.write("{};{}\n".format(record.name,c[0]))
                else:
                    output_fileid.write("{};{}\n".format(record.name,'undefined'))
            except ZeroDivisionError as e:
                output_fileid.write("{};{}\n".format(record.name,'none'))
    output_fileid.close()
        
def launch_blast_and_save_output (contigs_file, path, skipped_tigs = []):
    print("# blast computation")
    records = list(SeqIO.parse(contigs_file, "fasta"))
    genomic_by_length = []
    for record in records:
        print(record.name, "length", len(record))
        if record.name in skipped_tigs:
           print("\tdo not blast this tig")
        else:
            if len(record) > 1000000:
                print("\tsufficient length to be genomic")
                genomic_by_length.append(record.name)
            else:
                output_filename = os.path.join(path,"{}.xml".format(record.name))
                if os.path.exists(output_filename):
                    print("\tskipped")
                else:
                    result_handle = NCBIWWW.qblast("blastn", "nt", record.seq, megablast=True)

                    with open(output_filename, "w") as out_handle:
                        out_handle.write(result_handle.read())
                    result_handle.close()
                    out_handle.close()
                    
    return genomic_by_length
    
if __name__ == "__main__":
    for nctc_sample in open(sys.argv[1]):
        nctc_sample = nctc_sample.strip()

        #contigs_file = "assembly/{}/canu/canu.contigs.fasta".format(nctc_sample)
        contigs_file = "assembly/{}/miniasm/miniasm.fasta".format(nctc_sample)
        print("### NCTC sample number ", nctc_sample)
        sp = bact_dict[nctc_sample]["Species"][0]
        # get the path of the contigs_file
        contigs_file_path = os.path.dirname(contigs_file)
        genomic_by_length = launch_blast_and_save_output (contigs_file, contigs_file_path, skipped_tigs[nctc_sample])
        analyze_blast(contigs_file, contigs_file_path, sp, genomic_by_length)

