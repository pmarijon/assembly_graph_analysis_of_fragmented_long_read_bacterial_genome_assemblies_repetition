#!/usr/bin/env python3

import begin

import matplotlib.pyplot as plt

from matplotlib_venn import venn2


extremity_path = "{}/assembly_report/path_{}_extremity_result_filtred.csv"

def job(out_prefix, id_NCTC):
    canu_path = set()
    with open(extremity_path.format(id_NCTC, "canu")) as fh:
        fh.readline()
        for line in fh:
            line = line.split(",")
            canu_path.add(frozenset((line[0], line[2])))
    print(id_NCTC, canu_path)

    minimap_path = set()
    with open(extremity_path.format(id_NCTC, "minimap")) as fh:
        fh.readline()
        for line in fh:
            line = line.split(",")
            minimap_path.add(frozenset((line[0], line[2])))
    print(id_NCTC, minimap_path)

    venn2([canu_path, minimap_path], set_labels = ('Canu path', 'Minimap path'))
    plt.savefig(out_prefix + id_NCTC + ".png")
    plt.clf()

@begin.start
def main(out_prefix, *id_list):
    for id_NCTC in id_list:
        job(out_prefix, id_NCTC)
