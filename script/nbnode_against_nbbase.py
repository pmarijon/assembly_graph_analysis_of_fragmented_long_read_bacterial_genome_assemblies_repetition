#!/usr/bin/env python3

import os
import csv
import sys
import pandas
import argparse

from collections import defaultdict

import matplotlib.pyplot as plt
import seaborn as sns; sns.set(color_codes=True)

def main(args = None):
    
    if args is None:
        args = sys.argv[1:]

    parser = argparse.ArgumentParser(prog="assembly_report",
                                     formatter_class=argparse.
                                     ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--input",
                        help="File contain on SRA accession sample per line",
                        type=argparse.FileType("r"), required=True)
    parser.add_argument("-o", "--output",
                        help="output prefix",
                        type=str, required=True)

    args = vars(parser.parse_args(args))
    run(args["input"], args["output"])

    return 0

knot_report = "assembly/{}/report_filtred_AAG.csv"
read2position = "assembly/{}/report_filtred_knot/read2asm.paf"
assembly_report = "assembly/{}/assembly_report_new/find_path/path_minimap_extremity_result.csv"
def run(input_file, prefix):

    data = pandas.DataFrame()

    
    for nctc_id in input_file:
        nctc_id = nctc_id.strip()

        d = pandas.DataFrame()

        if not os.path.isfile(knot_report.format(nctc_id)) or not os.path.isfile(assembly_report.format(nctc_id)):
            continue
        knot = dict(get_data_knot(knot_report.format(nctc_id)))
        assembly = get_data_assembly(assembly_report.format(nctc_id), read2position.format(nctc_id))
        index = list(knot.keys() & assembly.keys())
        d["pairs"] = pandas.Series(index)
        d["dataset"] = pandas.Series([nctc_id]*len(index))
        d.set_index(["dataset", "pairs"], inplace=True)
        
        d["knot_read"] = pandas.Series([knot[k[1]]["nb_read"] for k in d.index], index=d.index, dtype=int)
        d["knot_base"] = pandas.Series([knot[k[1]]["nb_base"] for k in d.index], index=d.index, dtype=int)
        d["assembly_read"] = pandas.Series([assembly[k[1]]["nb_base"] for k in d.index], index=d.index, dtype=int)

        data = data.append(d)

    data = data[data["knot_read"] != 0]
        
    ax = sns.regplot(x="assembly_read", y="knot_read", data=data)
    ax.get_figure().savefig(prefix + "_assembly2knot_read.png")
    plt.clf()
    
    ax = sns.regplot(x="assembly_read", y="knot_base", data=data)
    ax.get_figure().savefig(prefix + "_assembly_read2_knot_base.png")
    plt.clf()
    
    ax = sns.regplot(x="knot_read", y="knot_base", data=data)
    ax.get_figure().savefig(prefix + "_read2base.png")
    plt.clf()
    
def get_data_knot(path):
    data = defaultdict(dict)
    with open(path) as handler:
        reader = csv.DictReader(handler)
        for r in reader:
            if r["paths"] == "not_found" and r["paths"] == "not_search":
                data[(r["tig1"], r["tig2"])]["nb_base"] = -1
                data[(r["tig1"], r["tig2"])]["nb_read"] = -1
                continue
            
            data[(r["tig1"], r["tig2"])]["nb_base"] = r["nb_base"]
            data[(r["tig1"], r["tig2"])]["nb_read"] = r["nb_read"]

    return data
            
def get_data_assembly(path, path_pos):
    data = defaultdict(dict)

    read2pos = dict()
    tig2len = dict()
    with open(path_pos) as handler:
        for line in handler:
            line = line.strip().split("\t")
            read2pos[line[0]] = int(line[7])
            tig2len[line[5]] = int(line[6])
   
    data = defaultdict(dict)
    with open(path) as handler:
        reader = csv.DictReader(handler)
        for r in reader:
            if tigname(r["tigA"]) not in tig2len or tigname(r["tigB"]) not in tig2len:
                continue

            if r["readA"][:-1] not in read2pos or r["readB"][:-1] not in read2pos:
                continue
            
            tig1 = ""
            if read2pos[r["readA"][:-1]] < tig2len[tigname(r["tigA"])]/2:
                tig1 = tigname(r["tigA"]) + "_begin"
            else:
                tig1 = tigname(r["tigA"]) + "_end"

            tig2 = ""
            if read2pos[r["readB"][:-1]] < tig2len[tigname(r["tigB"])]/2:
                tig2 = tigname(r["tigB"]) + "_begin"
            else:
                tig2 = tigname(r["tigB"]) + "_end"

            data[(tig1, tig2)]["nb_base"] = r["path_len"]

    return data

def tigname(base_name):
    return "tig"+base_name.rjust(8, "0")
    
if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
