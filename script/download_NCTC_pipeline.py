#!/usr/bin/env python3

import json
import os
import sys
import subprocess

base_dir = os.getcwd() + '/data/'
bact_dict = json.load(open('NCTC.json'))
    
cmd_base = 'wget ftp://ftp.sra.ebi.ac.uk/vol1/'

for bacterium_of_interest in open(sys.argv[1]):
    bacterium_of_interest = bacterium_of_interest.strip()
    dest_dir = base_dir+bacterium_of_interest+'/'
    os.system('mkdir -p '+dest_dir)

    for run, file_list in bact_dict[bacterium_of_interest]['file_paths'].items():
        for file_path in  file_list:
            cmd = cmd_base+file_path+' -P '+dest_dir
            print(cmd)
            os.system(cmd)

    dest_fasta_name = dest_dir+"/merged.fasta"

    dextract_cmd = 'dextract -o'+dest_fasta_name

    bax_files = [x for x in os.listdir(dest_dir) if x.endswith('.bax.h5')]

    for bax_file in bax_files:
        dextract_cmd +=  " " + dest_dir+bax_file

    try:
        print(dextract_cmd)
        subprocess.check_output(dextract_cmd.split())
        print('dextract done. deleting .bax.h5 files')
        os.system('rm '+dest_dir+'*.bax.h5')
        print('removing .quiva files')
        os.system('rm '+dest_dir+'*.quiva')
    except:
        print('error')



