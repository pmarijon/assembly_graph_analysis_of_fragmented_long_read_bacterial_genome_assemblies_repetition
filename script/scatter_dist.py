#!/usr/bin/env python3

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import os
import sys
import csv
import pysam
import pandas
import logging
import argparse
import requests
import itertools
import statistics

import seaborn as sns


from pathlib import Path
from collections import defaultdict

def main(args = None):
    
    if args is None:
        args = sys.argv[1:]

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s :: %(message)s')
    steam_handler = logging.StreamHandler()
    steam_handler.setFormatter(formatter)
    logger.addHandler(steam_handler)

    parser = argparse.ArgumentParser(prog="assembly_report",
                                     formatter_class=argparse.
                                     ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--input",
                        help="File contain on SRA accession sample per line",
                        type=argparse.FileType("r"), required=True)
    parser.add_argument("-d", "--data",
                        help="directory where sequence are download",
                        type=Path, required=True)
    parser.add_argument("-a", "--assembly",
                        help="directory where assembly are save",
                        type=Path, required=True)
    parser.add_argument("-j", "--json",
                        help="NCTC.json path",
                        type=argparse.FileType("r"), required=True)
    parser.add_argument("-o", "--output",
                        help="output prefix",
                        type=str, required=True)
    
    args = vars(parser.parse_args(args))
    finish(args)

    return 0

def finish(args):

    nctc = list()
    for nctc_id in args["input"]:
        nctc.append(nctc_id.strip())
    

    for i, nctc_id in enumerate(nctc):
        fig = plt.figure(num=None, figsize=(16, 12), dpi=240)
        ax = fig.add_subplot(111)
        path_dist = list()
        mapi_dist = list()
        label_list = list()

        print(nctc_id)

        assembly_len = get_assembly_len(nctc_id)
       
        data_path = args["data"] / nctc_id
        data_path.mkdir(parents=True, exist_ok=True)
        
        cov, mean_len = coverage(data_path, assembly_len)
       
        print("Coverage", cov)
        print("Mean length", mean_len)
        paths, read2tig = get_path(nctc_id)
       
        target_read = set()
        for r, s in paths.keys():
            target_read.add(r)
            target_read.add(s)

        reads2pos, ref_len = get_read_pos("assembly/{}/raw_read2ref.bam".format(nctc_id), target_read)

        for readA, readB in paths.keys():
            if "minimap" not in paths[(readA, readB)]:
                continue
            
            if readA not in reads2pos or readB not in reads2pos:
                continue

            a, b = (reads2pos[readA], ref_len - reads2pos[readB]) if reads2pos[readA] < reads2pos[readB] else (reads2pos[readB], ref_len - reads2pos[readA])
            c = abs(reads2pos[readA] - reads2pos[readB])
            ext_dist = a+b if a+b < c else c
            mapi_dist.append(ext_dist)
            path_dist.append(int((mean_len * paths[(readA, readB)]["minimap"])))
        
        ax.scatter(mapi_dist, path_dist)

        keep_x_dist = ax.get_xlim()
        keep_y_dist = ax.get_ylim()

        ax.set_xlim(0, 500000)
        ax.set_ylim(0, 500000)
        plt.xlabel("contig mapping distance")
        plt.ylabel("path estimate distance")
        plt.title(nctc_id+" estimate distance against real distance")
        plt.savefig(args["output"]+"/"+nctc_id+"_short_path.png")

        ax.scatter(mapi_dist, path_dist)
        ax.set_xlim(0, keep_x_dist[1]*1.05)
        ax.set_ylim(0, keep_y_dist[1]*1.05)
        plt.savefig(args["output"]+"/"+nctc_id+"_basic_dimension.png")
        
        ax.scatter(mapi_dist, path_dist)
        ax.set_xlim(keep_x_dist)
        ax.set_ylim(keep_x_dist)
        plt.savefig(args["output"]+"/"+nctc_id+"_x=y.png")

reference_template = "assembly/{}/reference.fasta"
def get_assembly_len(nctc_id):
    nb_base = 0

    with open(reference_template.format(nctc_id)) as fh:
        for line in fh:
            if not line.startswith(">"):
                nb_base += len(line)

    return nb_base

extremity_result_template = "assembly/{}/assembly_report/find_path/path_{}_extremity_result.csv"
def get_path(nctc_id):
    # (readA, readB) -> {"canu": len_canu, "minimap": len_minimap}
    read2tig = dict()
    paths = dict()
    with open(extremity_result_template.format(nctc_id, "canu")) as canu_fh:
       reader = csv.DictReader(canu_fh)
       for row in reader:
            key = (row["readA"][:-1], row["readB"][:-1])
            if key not in paths:
               paths[key] = dict()
               paths[key]["canu"] = int(row["path_len"])
            else:
                if paths[key]["canu"] < int(row["path_len"]):
                    paths[key]["canu"] = int(row["path_len"])
            read2tig[row["readA"][:-1]] = row["tigA"]
            read2tig[row["readB"][:-1]] = row["tigB"]

    with open(extremity_result_template.format(nctc_id, "minimap")) as minimap_fh:
       reader = csv.DictReader(minimap_fh)
       for row in reader:
            key = (row["readA"][:-1], row["readB"][:-1])
            if key not in paths:
               paths[key] = dict()
               paths[key]["minimap"] = int(row["path_len"])
            else:
                if "minimap" not in paths[key]:
                    paths[key]["minimap"] = int(row["path_len"])
                if paths[key]["minimap"] < int(row["path_len"]):
                    paths[key]["minimap"] = int(row["path_len"])
            
            read2tig[row["readA"][:-1]] = row["tigA"]
            read2tig[row["readB"][:-1]] = row["tigB"]

    return paths, read2tig

def coverage(data_path, assembly_len):
    nb_base = 0
    fastq_file = data_path / "merged.fasta"
    nb_base, nb_read = nb_base_in_file(str(fastq_file))

    coverage = nb_base / assembly_len 

    print("Nombre de base", nb_base)
    print("Nombre de read", nb_read)
    return coverage, nb_base / nb_read


def nb_base_in_file(path: Path):
    nb_base = 0
    
    line_count = 0
    path = os.path.abspath(str(path))
    with open(path, "r") as sequences:
        for line in sequences:
            line = line.strip()
            if line.startswith(">"):
                line_count += 1
            else:
                nb_base += len(line)

    return nb_base, line_count

extremity_search_template = "assembly/{}/assembly_report/find_path/path_{}_extremity_search.csv"
def get_possible_path(nctc_id, mapper):

    with open(extremity_search_template.format(nctc_id, mapper)) as canu_fh:
        reader = csv.DictReader(canu_fh)
        for row in reader:
            yield row["readA"][:-1], row["readB"][:-1]

readname_template = "assembly/{}/canu/unitigging/canu.gkpStore/readNames.txt"
def parse_canu_read_name(nctc_id):
    filename = readname_template.format(nctc_id)

    id2readname = dict()
    with open(filename, "r") as in_csv:
        reader = csv.reader(in_csv, delimiter='\t')
        for row in reader:
            id2readname[row[0]] = row[1].split(" ")[0]
    
    return id2readname

read2tig_template = "assembly/{}/canu/canu.contigs.layout.readToTig"
def parse_read_tig(nctc_id):
    with open(read2tig_template.format(nctc_id)) as in_csv:
        reader = csv.DictReader(in_csv, delimiter='\t')
        for row in reader:
            if row["bgn"] > row["end"]:
                row["bgn"], row["end"] = row["end"], row["bgn"]
            yield row["#readID"], row["tigID"], row["bgn"], row["end"]


def __get_map_pos(filename, select):

    mapping = pysam.AlignmentFile(filename, "rb")
    ref2length = {ref: mapping.get_reference_length(ref) for ref in mapping.references}
    selected_ref = sorted(ref2length.items(), key=lambda x: x[1], reverse=True)[0][0] 
 
    for read in mapping.fetch(reference=selected_ref):
        if read.reference_end - read.reference_start < read.query_length * 0.7:
            continue
        if read.flag & 2048:
            continue
        if read.query_name in select:
            yield read.query_name, read.reference_start, read.reference_end, read.query_length, ref2length[selected_ref]

def __clean_tigname(name):
    return name.replace("tig", "").lstrip("0")

def get_read_pos(filename, select):
    reads2pos = dict()
  
    ref_len = 0
    for k, beg, end, query_len, reflen in __get_map_pos(filename, select):
        reads2pos[k] = beg
        ref_len = reflen

    return reads2pos, ref_len


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
