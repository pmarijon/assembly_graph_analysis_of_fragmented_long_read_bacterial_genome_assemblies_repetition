#!/usr/bin/env python3

import os
import csv
import sys
import logging
import argparse
import itertools

import networkx as nx

from collections import defaultdict

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib import rcParams, cycler
import numpy as np

import pysam

from Bio import SeqIO

no_truth = {
    "NCTC5050",
    "NCTC9078",
    "NCTC7152",
}

default_truth = {
    "NCTC10006": [('55-', '55+'), ('1802-', '1802+')],
    "NCTC10332": [('1+', '1-'), ('76-', '76+'), ('49-', '49+')],
    "NCTC10444": [('105+', '105-'), ('671+', '671-'), ('673+', '673-'), ('675-', '675+')],
    "NCTC10702": [('1+', '1-'), ('2+', '2-')],
#   MISASSMEBLY
#    "NCTC10794": [('4951+', '4951-'), ('189-', '189+'), ('6-', '6+'), ('40-', '40+'), ('190+', '190-'), ('3-', '3+'), ('4952+', '4952-'), ('189+', '189-'), ('+', '-'), ('+', '-')]
#   CANU SOLVED
#    "NCTC11473": []
    "NCTC12123": [('1+', '1-'), ('2+', '2-'), ('9+', '9-')],
    "NCTC12132": [('1+', '1-'), ('2-', '2+')],
#   MISASSEMBLY
#    "NCTC12999": []
    "NCTC13125": [('3+', '3-'), ('408+', '408-'), ('1-', '1+')], # Petit problème a voir peut-être pas de réference NCTC valid
#   CANU SOLVED
#    "NCTC13145": [],
    "NCTC13463": [('66+', '66-'), ('68-', '68+')],
#   MISASSEMBLY
#    "NCTC4672": [],
#   MISASSEMBLY
#    "NCTC5053": [('-', '+'), ('-', '+'), ('+', '-'), ('+', '-'), ('-', '+'), ('+', '-')], # Petit pb a voir peut-être pas de ref NCTC valid
    "NCTC5055": [('1791+', '1791-'), ('1789+', '1789-')],
    #"NCTC8179": [('141+', '141-'), ('6+', '6-'), ('147-', '147+'), ('145-', '145+'), ('1523-', '1523+'), ('12-', '12+'), ('147+', '147-'), ('143+', '143-'), ('141+', '141-')], # petit misassembly
    "NCTC9075": [('1+', '1-'), ('2930+', '2930-'), ('129-', '129+'), ('14-', '14+'), ('55+', '55-'), ('196-', '196+')],
    "NCTC9078": [('1+', '1-'), ('51+', '51-')],
#   MISASSEMBLY probable
#    "NCTC9111": [('1+', '1-'), ('54-', '54+'), ('63-', '63+')],
#   MISASSEMBLY probable
#    "NCTC9098": [('1+', '1-'), ('526+', '526-'), ('528+', '528-'), ('530+', '530-')]
    "NCTC9112": [('707+', '707-'), ('705-', '705+'), ('65+', '65-'), ('84-', '84+')],
    "NCTC9596": [('76+', '76-'), ('3+', '3-'), ('4+', '4-'), ('78+', '78-'), ('74+', '74-')]
}

for t in default_truth:
    pos_min = default_truth[t].index(min(default_truth[t]))
    default_truth[t] = default_truth[t][pos_min:] + default_truth[t][:pos_min]


def main(args = None):
    
    if args is None:
        args = sys.argv[1:]

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s :: %(message)s')
    steam_handler = logging.StreamHandler()
    steam_handler.setFormatter(formatter)
    logger.addHandler(steam_handler)

    parser = argparse.ArgumentParser(prog="assembly_report",
                                     formatter_class=argparse.
                                     ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--input",
                        help="File contain on SRA accession sample per line",
                        type=argparse.FileType("r"), required=True)
    parser.add_argument("-k", "--knot",
                        help="read knot result",
                        action="store_true")
    parser.add_argument("-o", "--output",
                        help="File where image are write",
                        type=str, required=True)

    args = vars(parser.parse_args(args))
    run(args)

    return 0

def run(args):

    output_csv = open(args["output"]+".csv", "w")
    print("NCTCID,nb_of_path,weigth_of_each_path,true_position", file=output_csv)
        
    fig = plt.figure()
    ax = plt.axes()

    id_base_nbcontig = dict()

    nb_truth = 0
    nb_truth_0 = 0

    id2curve = dict() 

    for nctc_id in args["input"]:
        nctc_id = nctc_id.strip()

        graph, reject_contig = get_graph(nctc_id, args["knot"])

        print(graph.nodes)
        print(graph.edges)
        print(nctc_id)
        print("\t nb edges :", len(graph.edges))
        print("\t nb nodes :", len(graph.nodes))
        
        if len(graph.edges) == 0:
            print("\t graph empty")
            continue

        if len(graph.edges) > 100 and nctc_id not in default_truth:
            print("\t graph to large")
            continue
        

        contig_order = get_contig_order(graph)
        weight = [weight for weight, _ in contig_order]

        print("\t nb path :", len(contig_order))
        if len(contig_order) == 0:
            print("\t no path")
            continue
        
        tt_base = 0
        for _, nb_base in reject_contig:
            tt_base += nb_base
        id_base_nbcontig[nctc_id] = (len(reject_contig), tt_base)

        mark_point = []
        if nctc_id in default_truth:
            mark_point = good_contig_order(nctc_id, contig_order)
            nb_truth += 1
            if mark_point != [0]:
                print("not the first hamilton :(")
                nb_truth_0 += 1
            else:
                print("the first hamilton :)")

        print("\t", contig_order)
        id2curve[nctc_id] = [len(contig_order), weight, mark_point]
    
    C = np.linspace(0, 1, len(id2curve))
    cmap = plt.cm.viridis
    for i, (n_id, val) in enumerate(id2curve.items()):
        (c_o, w, m) = val
        ax.plot(range(1, c_o+1), w, label=n_id, markevery=m, marker="D", c=cmap(C[i]))
        
        weight_string = ";".join([str(v) for v in w])
        true_position = "-1" if len(m) == 0 else str(m[0])
        print(",".join([n_id, str(c_o), weight_string, true_position]), file=output_csv)
    
    ax.add_patch(patches.Rectangle((0, -1), 3, 300000, color="navajowhite"))

    ax.legend(prop={'size': 8})
    ax.set_xlim(0, 10)
    ax.set_xticks(range(1, 10))
    ax.text(0.2, 250000, "Top 3", fontsize=12)
    ax.set_ylim(-1, 300000)
    plt.xlabel("scenarios sorted by weight")
    plt.ylabel("Hamiltonian path weight")
    
    
    plt.savefig(args["output"])


    # Plot other thing
    fig = plt.figure()
    ax = plt.axes()

    x, y, label = list(), list(), list()
    for nctc_id, (nb_contig, nb_base) in id_base_nbcontig.items():
        x.append(nb_base)
        y.append(nb_contig)
        label.append(nctc_id)
        ax.plot(nb_base, nb_contig, 'bo', label=nctc_id)

    #ax.plot(x, y, 'bo', label=label)
    ax.legend()
    plt.xlabel("nombre de base (bp)")
    plt.ylabel("nombre de contig")
    plt.savefig(args["output"] + "_remove_contig")

    print("Number of first / number of thrut: {}/{}".format(nb_truth_0, nb_truth))

def good_contig_order(nctc_id, contig):

    thrut = default_truth[nctc_id]

    rev_thrut = [(t[1], t[0]) for t in reversed(thrut)]
   
    print(thrut, rev_thrut)
    
    pos_min = thrut.index(min(thrut))
    thrut = thrut[pos_min:] + thrut[:pos_min]
    thrut = tuple(itertools.chain(*thrut))
    #print("\t true contig order :", thrut)

    pos_min = rev_thrut.index(min(rev_thrut))
    rev_thrut = rev_thrut[pos_min:] + rev_thrut[:pos_min]
    rev_thrut = tuple(itertools.chain(*rev_thrut))
    #print("\t reverse true contig order :", rev_thrut)

    if len(thrut) != len(contig[0][1]):
        print("\tthrut and nb of contig is diff")
        return []

    for i, (_, t) in enumerate(contig):
        if t == thrut or t == rev_thrut:
            return [i]
    
    return []

def get_contig_order(graph):

    min_weight = float("inf")
    min_cycle = None

    weight_cycle = sorted([(weight_of_cycle(graph, cycle), cycle) for cycle in valid_cycle(graph)])
       
    return weight_cycle

def valid_cycle(graph):
    for cycle in normalized_permutations(itertools.permutations(graph.nodes())):
        if any([edge not in graph.edges for edge in zip(cycle, cycle[1:] + cycle[:1])]):
            continue

        yield cycle

def contig_congruence_check(g):
    for i, j in zip(g[::2], g[1::2]):
        if i[:-1] != j[:-1]:
            return False
    return True

def normalized_permutations(generator):
    for g in generator:
        if g.index(min(g)) == 0 and contig_congruence_check(g):
            yield g

def weight_of_cycle(graph, path):
    weight = 0
    for edge in zip(path, path[1:] + path[:1]):
        weight += graph.edges[edge[0], edge[1]]["weight"]

    return weight

def __get_tigname(row, name, read2tig, tig2read):
    read = row["read" + name][:-1]

    if read in read2tig:
        return read2tig[read]
    else:
        if row["tig" + name] + "+" in tig2read:
            tig = row["tig" + name] + "-"
        else:
            tig = row["tig" + name] + "+"

        read2tig[read] = tig
        tig2read[tig] = read

    return tig

def get_graph(nctc_id, knot):
    if knot:
        return get_graph_knot(nctc_id)
    else:
        return get_graph_assembly(nctc_id)

contig_file = "assembly/{}/canu/canu.contigs.fasta"
knot_result_template = "assembly/{}/report_filtred_unbiased_AAG.csv"
def get_graph_knot(nctc_id):
    G = nx.DiGraph()

    valid_contig = set()
    reject_contig = set()
    for record in SeqIO.parse(contig_file.format(nctc_id), "fasta"):
        if len(record.seq) > 100000:
            valid_contig.add(norm_contigname(record.id))
        else:
            reject_contig.add((norm_contigname(record.id), len(record.seq)))

    print(valid_contig)
    if not os.path.isfile(knot_result_template.format(nctc_id)):
        return G, reject_contig

    paths = dict()
    
    with open(knot_result_template.format(nctc_id)) as knot_fh:
        reader = csv.DictReader(knot_fh)
        for row in reader:
            if row["paths"] == "not_found":
                continue
            
            tiga = row["tig1"].split("_")
            tigA = norm_contigname(tiga[0])
            tigA += "+" if tiga[1] == "begin" else "-"
            tigb = row["tig2"].split("_")
            tigB = norm_contigname(tigb[0])
            tigB += "+" if tigb[1] == "begin" else "-"

            if tigA[:-1] not in valid_contig or tigB[:-1] not in valid_contig:
                continue
            
            if not G.has_edge(tigA[:-1] + "+", tigA[:-1] + "-"):
                G.add_edge(tigA[:-1] + "+", tigA[:-1] + "-", weight=0.0)
            if not G.has_edge(tigA[:-1] + "-", tigA[:-1] + "+"):
                G.add_edge(tigA[:-1] + "-", tigA[:-1] + "+", weight=0.0)
            if not G.has_edge(tigB[:-1] + "+", tigB[:-1] + "-"):
                G.add_edge(tigB[:-1] + "+", tigB[:-1] + "-", weight=0.0)
            if not G.has_edge(tigB[:-1] + "-", tigB[:-1] + "+"):
                G.add_edge(tigB[:-1] + "-", tigB[:-1] + "+", weight=0.0)            

            if float(row["nb_base"]) < 0:
                nb_base = 0
            else:
                nb_base = float(row["nb_base"])
                
            if frozenset((tigA, tigB)) in paths and paths[frozenset((tigA, tigB))] < nb_base:
                continue
            paths[frozenset((tigA, tigB))] = nb_base

        for (tigA, tigB) in paths.keys():
            G.add_edge(tigA, tigB, weight=paths[frozenset((tigA, tigB))]) 
            G.add_edge(tigB, tigA, weight=paths[frozenset((tigA, tigB))])
            
    return G, reject_contig
            
contig_file = "assembly/{}/canu/canu.contigs.fasta"
extremity_result_template = "assembly/{}/assembly_report_new/find_path/path_{}_extremity_result.csv"
def get_graph_assembly(nctc_id):
    G = nx.DiGraph()

    read2tig = dict()
    tig2read = dict()

    valid_contig = set()
    reject_contig = set()
    for record in SeqIO.parse(contig_file.format(nctc_id), "fasta"):
        if len(record.seq) > 100000:
            valid_contig.add(norm_contigname(record.id))
        else:
            reject_contig.add((norm_contigname(record.id), len(record.seq)))

    path = defaultdict(lambda: float("inf"))
    with open(extremity_result_template.format(nctc_id, "minimap")) as minimap_fh:
        reader = csv.DictReader(minimap_fh)
        for row in reader:

            tigA = __get_tigname(row, "A", read2tig, tig2read) 
            tigB = __get_tigname(row, "B", read2tig, tig2read) 

            if tigA[:-1] not in valid_contig or tigB[:-1] not in valid_contig:
                continue

            if not G.has_edge(tigA[:-1] + "+", tigA[:-1] + "-"):
                G.add_edge(tigA[:-1] + "+", tigA[:-1] + "-", weight=0.0)
            if not G.has_edge(tigA[:-1] + "-", tigA[:-1] + "+"):
                G.add_edge(tigA[:-1] + "-", tigA[:-1] + "+", weight=0.0)
            if not G.has_edge(tigB[:-1] + "+", tigB[:-1] + "-"):
                G.add_edge(tigB[:-1] + "+", tigB[:-1] + "-", weight=0.0)
            if not G.has_edge(tigB[:-1] + "-", tigB[:-1] + "+"):
                G.add_edge(tigB[:-1] + "-", tigB[:-1] + "+", weight=0.0)

            G.add_edge(tigA, tigB, weight=float(row["path_len"]))

    return G, reject_contig

def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args, fillvalue=fillvalue)

def norm_contigname(tigname):
    tigname = tigname[3:]
    tigname = tigname.lstrip('0')
    return tigname

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
