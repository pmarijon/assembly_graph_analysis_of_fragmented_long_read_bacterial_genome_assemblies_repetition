#! /bin/bash

for i in $(cat ${1});
do
    echo -n $i
    #cut -d';' -f2 ./assembly/$i/taxonomic_assign.lst | sort | uniq -c | tr '\n' ' '
    cut -d';' -f2 ./assembly/$i/taxonomic_assign_miniasm.lst | sort | uniq -c | tr '\n' ' '
    echo ""
done
