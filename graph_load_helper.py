import gfapy

import networkx as nx

from path_in_gfa.extract_dag import *
from path_in_gfa.utils import *

def load(path):
    gfa = gfapy.Gfa.from_file(path)
    G = nx.DiGraph()

    populate_graph(gfa, G)

    G = transitive_reduction(G)

    return G
